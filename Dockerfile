FROM maven AS build
COPY src /main/app/src
COPY pom.xml /main/app

RUN mvn -f /main/app/pom.xml clean package -DSkipTests

FROM openjdk:19
EXPOSE 8082
COPY --from=build /main/app/target/springboottask-0.0.1-SNAPSHOT.jar /app/app.jar

CMD docker create -v /var/lib/postgresql/data --name PostgresData alpine
CMD ["java","-jar","app/app.jar"]





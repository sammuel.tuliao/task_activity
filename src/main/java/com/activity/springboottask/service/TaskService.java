package com.activity.springboottask.service;

import com.activity.springboottask.controller.TaskRestController;
import com.activity.springboottask.dto.DtoTask;
import com.activity.springboottask.entity.Task;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskService {

 private final TaskRestController taskRestController;

    public TaskService(TaskRestController taskRestController) {
        this.taskRestController = taskRestController;
    }

    public List<DtoTask> getAllTask(){
        List<Task> task = taskRestController.getAllTask();
        return task.stream().map((tasks) ->
               maptoDtoTask(tasks)).collect((Collectors.toList()) );
    }

    public DtoTask getById (int task_id){
        DtoTask task = maptoDtoTask(taskRestController.getById(task_id));

        return task;
    }

    private DtoTask maptoDtoTask(Task task){
        DtoTask dtoTask = new DtoTask();
        dtoTask.setTask_id(task.getTask_id());
        dtoTask.setTitle(task.getTitle());
        dtoTask.setDescription(task.getDescription());
        dtoTask.setCompleted(task.isCompleted());
        return dtoTask;
    }

    public void addTask(DtoTask dtoTask){
        Task task = new Task();
        task.setTitle(dtoTask.getTitle());
        task.setDescription(dtoTask.getDescription());
        task.setCompleted(false);
        taskRestController.AddTask(task);
    }

    public void updateTask(DtoTask dtoTask){
        Task task = new Task();
        task.setTask_id(dtoTask.getTask_id());
        task.setTitle(dtoTask.getTitle());
        task.setDescription(dtoTask.getDescription());
        task.setCompleted(dtoTask.isCompleted());
        taskRestController.updateTask(task.getTask_id(),task);
    }

    public void deleteTask(int task_id){
        taskRestController.deleteTask(task_id);
    }






}

package com.activity.springboottask.controller;

import com.activity.springboottask.dto.DtoTask;
import com.activity.springboottask.service.TaskService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/web")
public class TaskController {


    private final TaskService taskService;


    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/tasks")
    public String taskList (Model model){
        model.addAttribute("tasks", taskService.getAllTask());
        return "task/tasks";
    }
    @GetMapping("/addTask")
    public String showAddTaskForm(Model model){
        DtoTask dtoTask = new DtoTask();
        model.addAttribute("tasks", dtoTask);
        return "task/add";
    }

    @PostMapping("/addTask/save")
    public String addTask(@Validated @ModelAttribute("tasks") DtoTask dtoTask) {

        taskService.addTask(dtoTask);
        return "redirect:/web/addTask?success";
    }

    @GetMapping("/edit/{task_id}")
    public String ShowUpdateTask(@PathVariable int task_id, Model model){
        DtoTask dtoTask = taskService.getById(task_id);
        model.addAttribute("editInput", dtoTask );
        return "task/edit";
    }

    @PostMapping("/edit/save/{task_id}")
    public String editTask(@PathVariable int task_id,@Validated @ModelAttribute("editInput") DtoTask dtoTask){
       taskService.updateTask(dtoTask);
       return "redirect:/web/tasks";

    }

    @GetMapping("/delete/{task_id}")
    public String deleteTask(@PathVariable int task_id){
        taskService.deleteTask(task_id);
    return "redirect:/web/tasks";
    }



}

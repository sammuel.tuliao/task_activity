package com.activity.springboottask.controller;

import com.activity.springboottask.entity.Task;
import com.activity.springboottask.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TaskRestController {

    @Autowired
    private TaskRepository taskRepository;

@PostMapping("/addtask")
public Task AddTask(Task task){
    return taskRepository.save(task);
}

@GetMapping("/tasks")
public List<Task> getAllTask(){
    return taskRepository.findAll();
}

@GetMapping("/onetask/{task_id}")
public Task getById (@PathVariable int task_id){
    return taskRepository.findBytaskId(task_id);
    }

@PutMapping("/update/{task_id}")
    public ResponseEntity<Task> updateTask(@PathVariable int task_id,@RequestBody Task taskDetails ){
    Task task = taskRepository.findById((long)task_id).orElseThrow();
    task.setTitle(taskDetails.getTitle());
    task.setDescription(taskDetails.getDescription());
    task.setCompleted(taskDetails.isCompleted());
     return new ResponseEntity<>(taskRepository.save(task), HttpStatus.OK);
}

@DeleteMapping("/delete/{task_id}")
    public ResponseEntity<Task> deleteTask(@PathVariable int task_id){
    Task task = taskRepository.findById((long)task_id).orElseThrow();
    taskRepository.delete(task);
        return ResponseEntity.ok().build();
}
}

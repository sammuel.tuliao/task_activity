package com.activity.springboottask.repository;

import com.activity.springboottask.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
        Task findBytaskId(int task_id);
//    List<Task> deleteBytask_id(int student_id);
}
